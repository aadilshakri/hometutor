package com.tutor.aadil;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ConfigurableApplicationContext;

@SpringBootApplication
public class AadilApplication {

	public static void main(String[] args) {

		ConfigurableApplicationContext ctx=SpringApplication.run(AadilApplication.class, args);
//		String[] beans = ctx.getBeanDefinitionNames();
//		for(String s : beans) System.out.println(s);
	}

}
