package com.tutor.aadil.controller;

import com.tutor.aadil.exceptions.NullValueException;
import com.tutor.aadil.exceptions.StudentAlreadyExists;
import com.tutor.aadil.exceptions.StudentNotFoundException;
import com.tutor.aadil.model.Student;
import com.tutor.aadil.service.StudentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@CrossOrigin("*")
@RequestMapping("/student")
public class StudentController {

    @Autowired
    private StudentService studentService;

    ResponseEntity responseEntity;

    @PostMapping("/addStudent")
    public ResponseEntity<?> addStudent(@RequestBody Student student) throws StudentAlreadyExists, NullValueException {
        String studentID= student.getStudentID();
        String name = student.getName();
        String emailId = student.getEmailId();
        String password=student.getPassword();
        int age= student.getAge();
        responseEntity= new ResponseEntity<Student>(studentService.addStudent(studentID, name, emailId,password,age), HttpStatus.OK);
        return responseEntity;
    }

    @GetMapping("/getStudentByID/{studentID}")
    public ResponseEntity<?> getStudentByID(@PathVariable String studentID) throws StudentNotFoundException {
        responseEntity= new ResponseEntity<Student>(studentService.getStudentByStudentID(studentID),HttpStatus.OK);
        return responseEntity;
    }

    @GetMapping("/getStudentByEmailId/{emailId}")
    public ResponseEntity<?> getStudentByEmailId(@PathVariable String emailId) throws StudentNotFoundException {
        responseEntity= new ResponseEntity<Student>(studentService.getStudentByEmailId(emailId),HttpStatus.OK);
        return responseEntity;
    }

    @GetMapping("/getAllStudents")
    public ResponseEntity<?> getAllStudents() throws NullValueException {
        responseEntity=new ResponseEntity<List<Student>>(studentService.getAllStudents(),HttpStatus.OK);
        return responseEntity;
    }

    @PutMapping("/updateStudent")
    public ResponseEntity<?> updateStudent(@RequestBody Student student) throws StudentNotFoundException{
        String studentID= student.getStudentID();
        String name = student.getName();
        String emailId = student.getEmailId();
        String password=student.getPassword();
        int age= student.getAge();
        responseEntity= new ResponseEntity<Student>(studentService.updateStudent(studentID, name, emailId, password,age), HttpStatus.OK);
        return responseEntity;
    }

    @DeleteMapping("/deleteStudentByEmailId/{emailId}")
    public ResponseEntity<?> deleteStudentByEmailId(@PathVariable String emailId) throws StudentNotFoundException{
        studentService.deleteStudentByEmailId(emailId);
        responseEntity= new ResponseEntity<Void>(HttpStatus.OK);
        return responseEntity;
    }

    @DeleteMapping("/deleteAllStudents")
    public ResponseEntity<?> deleteAll() throws NullValueException{
        studentService.deleteAll();
        responseEntity= new ResponseEntity<Void>(HttpStatus.OK);
        return responseEntity;
    }
}
