package com.tutor.aadil.controller;


import com.tutor.aadil.exceptions.NullValueException;
import com.tutor.aadil.exceptions.TutorAlreadyExists;
import com.tutor.aadil.exceptions.TutorNotFoundException;
import com.tutor.aadil.model.Tutor;
import com.tutor.aadil.service.TutorService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@CrossOrigin("*")
@RequestMapping("/tutor")
public class TutorController {

    @Autowired
    private TutorService tutorService;

    ResponseEntity responseEntity;

    @PostMapping("/addTutor")
    public ResponseEntity<?> addTutor(@RequestBody Tutor tutor) throws NullValueException, TutorAlreadyExists {
        String tutorID= tutor.getTutorID();
        String name = tutor.getName();
        String emailId = tutor.getEmailId();
        String password=tutor.getPassword();
        String occupation = tutor.getOccupation();
        String degree = tutor.getDegree();
        String subjects= tutor.getSubjects();
        responseEntity= new ResponseEntity<Tutor>(tutorService.addTutor(tutorID, name, emailId,password,occupation, degree,subjects), HttpStatus.OK);
        return responseEntity;
    }

    @GetMapping("/getTutorByID/{tutorID}")
    public ResponseEntity<?> getTutorByID(@PathVariable String tutorID) throws TutorNotFoundException {
        responseEntity= new ResponseEntity<Tutor>(tutorService.getTutorByTutorID(tutorID),HttpStatus.OK);
        return responseEntity;
    }

    @GetMapping("/getTutorByName/{name}")
    public ResponseEntity<?> getTutorByName(@PathVariable String name) throws TutorNotFoundException {
        responseEntity= new ResponseEntity<List<Tutor>>(tutorService.getTutorByName(name),HttpStatus.OK);
        return responseEntity;
    }

    @GetMapping("/getAllTutors")
    public ResponseEntity<?> getAllTutors(){
        responseEntity=new ResponseEntity<List<Tutor>>(tutorService.getAllTutors(),HttpStatus.OK);
        return responseEntity;
    }

    @PutMapping("/updateTutor")
    public ResponseEntity<?> updateTutor(@RequestBody Tutor tutor) throws TutorNotFoundException{
        String tutorID= tutor.getTutorID();
        String name = tutor.getName();
        String emailId = tutor.getEmailId();
        String password=tutor.getPassword();
        String occupation = tutor.getOccupation();
        String degree = tutor.getDegree();
        String subjects= tutor.getSubjects();
        responseEntity= new ResponseEntity<Tutor>(tutorService.updateTutor(tutorID, name, emailId, password,occupation, degree,subjects), HttpStatus.OK);
        return responseEntity;
    }

    @DeleteMapping("/deleteTutorByName/{name}")
    public ResponseEntity<?> deleteTutorByName(@PathVariable String name) throws TutorNotFoundException{
        tutorService.deleteTutorByName(name);
        responseEntity= new ResponseEntity<Void>(HttpStatus.OK);
        return responseEntity;
    }

    @DeleteMapping("/deleteAllTutors")
    public ResponseEntity<?> deleteAll(){
        tutorService.deleteAll();
        responseEntity= new ResponseEntity<Void>(HttpStatus.OK);
        return responseEntity;
    }

}
