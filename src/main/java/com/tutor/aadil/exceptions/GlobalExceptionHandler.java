package com.tutor.aadil.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

@ControllerAdvice
public class GlobalExceptionHandler {

    ResponseEntity responseEntity;

    @ExceptionHandler(NullValueException.class)
    public ResponseEntity<String> NullValueException(final NullValueException e){
        return responseEntity=new ResponseEntity<String>(e.getMessage(), HttpStatus.CONFLICT);
    }

    @ExceptionHandler(StudentNotFoundException.class)
    public ResponseEntity<String> StudentNotFoundException(final StudentNotFoundException e){
        return responseEntity=new ResponseEntity<String>(e.getMessage(),HttpStatus.CONFLICT);
    }

    @ExceptionHandler(StudentAlreadyExists.class)
    public ResponseEntity<String> StudentAlreadyExists(final StudentAlreadyExists e){
        return responseEntity=new ResponseEntity<String>(e.getMessage(),HttpStatus.CONFLICT);
    }

    @ExceptionHandler(TutorAlreadyExists.class)
    public ResponseEntity<String> TutorAlreadyExists(final TutorAlreadyExists e){
        return responseEntity=new ResponseEntity<String>(e.getMessage(),HttpStatus.CONFLICT);
    }

    @ExceptionHandler(TutorNotFoundException.class)
    public ResponseEntity<String> TutorNotFoundException(final TutorNotFoundException e){
        return responseEntity=new ResponseEntity<String>(e.getMessage(),HttpStatus.CONFLICT);
    }

}
