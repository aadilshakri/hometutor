package com.tutor.aadil.exceptions;

public class StudentAlreadyExists extends Exception {

    private String message;

    StudentAlreadyExists(){}

    public StudentAlreadyExists(String message){
        super(message);
        this.message=message;

    }
}
