package com.tutor.aadil.exceptions;

public class StudentNotFoundException extends Exception {

    private String message;

    public StudentNotFoundException(){}

    public StudentNotFoundException(String message){
        super(message);
        this.message=message;
    }
}
