package com.tutor.aadil.exceptions;

public class TutorAlreadyExists extends Exception{

    private String message;

   TutorAlreadyExists(){}

   public TutorAlreadyExists(String message){
       super(message);
       this.message=message;
   }

}
