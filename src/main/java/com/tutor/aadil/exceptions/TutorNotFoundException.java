package com.tutor.aadil.exceptions;

public class TutorNotFoundException extends Exception{

    private String message;

    public TutorNotFoundException(){}

    public TutorNotFoundException(String message){
        super(message);
        this.message=message;
    }
}
