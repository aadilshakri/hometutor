package com.tutor.aadil.model;

import java.io.Serializable;

public class JwtRequest implements Serializable {

    private String emailId;
    private String password;

    public JwtRequest()
    {

    }

    public JwtRequest(String emailId, String password) {
        this.setUsername(emailId);
        this.setPassword(password);
    }

    public String getEmailId() {

        return this.emailId;
    }

    public void setUsername(String username) {

        this.emailId = emailId;
    }

    public String getPassword() {

        return this.password;
    }

    public void setPassword(String password) {

        this.password = password;
    }
}
