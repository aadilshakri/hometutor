package com.tutor.aadil.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;
import java.time.LocalDateTime;

@AllArgsConstructor
@NoArgsConstructor
@Builder
@Data
@Document
public class Tutor {

    @Id
    private String tutorID;
    private String name;
    private LocalDateTime timeStamp;
    private String subjects;
    private String emailId;
    private String password;
    private String occupation;
    private String degree;

}
