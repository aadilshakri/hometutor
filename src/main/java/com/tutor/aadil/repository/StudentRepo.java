package com.tutor.aadil.repository;

import com.tutor.aadil.model.Student;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface StudentRepo extends MongoRepository<Student, String> {

    Student findByStudentID(String studentID);

    Student findByName(String name);

    Student findByEmailId(String emailId);
}
