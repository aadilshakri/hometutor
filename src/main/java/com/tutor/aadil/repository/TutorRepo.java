package com.tutor.aadil.repository;

import com.tutor.aadil.model.Tutor;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface TutorRepo extends MongoRepository<Tutor, String> {

    Tutor findByTutorID(String tutorID);

    List<Tutor> findByName(String name);

    Tutor findByEmailId(String emailId);
}
