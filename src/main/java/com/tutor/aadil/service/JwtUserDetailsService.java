package com.tutor.aadil.service;

import com.tutor.aadil.model.Student;
import com.tutor.aadil.repository.StudentRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import java.util.ArrayList;

import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;

@Service
public class JwtUserDetailsService implements UserDetailsService  {

    @Autowired
    StudentRepo studentRepo;

    @Autowired
    private PasswordEncoder bcryptEncoder;

    @Override
    public UserDetails loadUserByUsername(String emailId) throws UsernameNotFoundException {

        Student student = studentRepo.findByEmailId(emailId);
        if (student == null) {
            throw new UsernameNotFoundException("Tutor not found with emailId: " + emailId);
        }
        return new org.springframework.security.core.userdetails.User(student.getEmailId(), student.getPassword(),
                new ArrayList<>());
    }
}
