package com.tutor.aadil.service;

import com.tutor.aadil.exceptions.NullValueException;
import com.tutor.aadil.exceptions.StudentAlreadyExists;
import com.tutor.aadil.exceptions.StudentNotFoundException;
import com.tutor.aadil.model.Student;
import java.util.List;

public interface StudentService {

    Student getStudentByStudentID(String studentID) throws StudentNotFoundException;

    Student getStudentByEmailId(String emailId) throws StudentNotFoundException;

    List<Student> getAllStudents() throws NullValueException;

    Student addStudent(String studentID,String name,String emailId,String password, int age) throws NullValueException, StudentAlreadyExists;

    Student updateStudent(String studentID,String name,String emailId,String password, int age) throws StudentNotFoundException;

    void deleteStudentByEmailId(String name) throws StudentNotFoundException;

    void deleteAll() throws NullValueException;
}
