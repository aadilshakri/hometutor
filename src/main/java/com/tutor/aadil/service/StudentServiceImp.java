package com.tutor.aadil.service;

import com.tutor.aadil.exceptions.NullValueException;
import com.tutor.aadil.exceptions.StudentAlreadyExists;
import com.tutor.aadil.exceptions.StudentNotFoundException;
import com.tutor.aadil.model.Student;
import com.tutor.aadil.repository.StudentRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import java.util.List;

@Service
public class StudentServiceImp implements StudentService{

    @Autowired
    StudentRepo studentRepo;

    @Autowired
    private PasswordEncoder bcryptEncoder;

    @Override
    public Student getStudentByStudentID(String studentID) throws StudentNotFoundException {
        Student student= studentRepo.findByStudentID(studentID);
        if(student==null){
            throw new StudentNotFoundException("Student not found");
        }
        return student;
    }

    @Override
    public Student getStudentByEmailId(String emailId) throws StudentNotFoundException {
        Student student= studentRepo.findByEmailId(emailId);
        if(student==null){
            throw new StudentNotFoundException("Student not found.");
        }
        return student;
    }

    @Override
    public List<Student> getAllStudents() throws NullValueException {

        return studentRepo.findAll();
    }

    @Override
    public Student addStudent(String studentID,String name,String emailId,String password,int age) throws NullValueException, StudentAlreadyExists {

        if(emailId==null || name==null){
            throw new NullValueException("Student emailId or name is null.");
        }
        if(studentRepo.findByEmailId(emailId)!=null){
            throw new StudentAlreadyExists("Student emailId already exists.");
        }

        Student student= new Student();
        student.setStudentID(studentID);
        student.setName(name);
        student.setEmailId(emailId);
        student.setPassword(bcryptEncoder.encode(password));
        student.setAge(age);
        studentRepo.save(student);
        return student ;
    }

    @Override
    public Student updateStudent(String studentID,String name,String emailId,String password,int age) throws StudentNotFoundException{

        if(studentRepo.findByEmailId(emailId)==null){
            throw new StudentNotFoundException("Student not found.");
        }
        Student student= new Student();
        student.setStudentID(studentID);
        student.setName(name);
        student.setEmailId(emailId);
        student.setPassword(bcryptEncoder.encode(password));
        student.setAge(age);
        studentRepo.save(student);
        return student ;
    }

    @Override
    public void deleteStudentByEmailId(String emailId) throws StudentNotFoundException{
        Student student= studentRepo.findByEmailId(emailId);
        if(student==null){
            throw new StudentNotFoundException("Student not found");
        }
        studentRepo.delete(student);
    }

    @Override
    public void deleteAll() throws NullValueException {
        studentRepo.deleteAll();
    }
}
