package com.tutor.aadil.service;

import com.tutor.aadil.exceptions.NullValueException;
import com.tutor.aadil.exceptions.TutorAlreadyExists;
import com.tutor.aadil.exceptions.TutorNotFoundException;
import com.tutor.aadil.model.Tutor;

import java.util.List;

public interface TutorService {

    Tutor getTutorByTutorID(String tutorID) throws TutorNotFoundException;

    List<Tutor> getTutorByName(String name) throws TutorNotFoundException;

    List<Tutor> getAllTutors();

    Tutor addTutor(String tutorID,String name,String emailId,String password, String occupation, String degree,String subjects) throws NullValueException, TutorAlreadyExists;

    Tutor updateTutor(String tutorID,String name,String emailId,String password, String occupation, String degree,String subjects) throws TutorNotFoundException;

    void deleteTutorByName(String name) throws TutorNotFoundException;

    void deleteAll();

}
