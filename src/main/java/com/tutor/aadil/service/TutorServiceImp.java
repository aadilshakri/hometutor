package com.tutor.aadil.service;

import com.tutor.aadil.exceptions.NullValueException;
import com.tutor.aadil.exceptions.StudentNotFoundException;
import com.tutor.aadil.exceptions.TutorAlreadyExists;
import com.tutor.aadil.exceptions.TutorNotFoundException;
import com.tutor.aadil.model.Tutor;
import com.tutor.aadil.repository.TutorRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.List;

@Service
public class TutorServiceImp implements TutorService{

    @Autowired
    TutorRepo tutorRepo;

    @Autowired
    private PasswordEncoder bcryptEncoder;

    @Override
    public Tutor getTutorByTutorID(String tutorID) throws TutorNotFoundException {
        Tutor tutor= tutorRepo.findByTutorID(tutorID);
        if(tutor==null){
            throw new TutorNotFoundException("Tutor not found");
        }
        return tutor;
    }

    @Override
    public List<Tutor> getTutorByName(String name) throws TutorNotFoundException{
        List<Tutor> tutor= tutorRepo.findByName(name);
        if(tutor.isEmpty()){
            throw new TutorNotFoundException("Tutor not found.");
        }
        return tutor;
    }

    @Override
    public List<Tutor> getAllTutors() {
        return tutorRepo.findAll();
    }

    @Override
    public Tutor addTutor(String tutorID,String name,String emailId,String password, String occupation, String degree,String subjects) throws NullValueException, TutorAlreadyExists {

        if(name==null || emailId==null){
            throw new NullValueException("Tutor name or emailId is null.");
        }

        if(tutorRepo.findByEmailId(emailId)!=null){
            throw new TutorAlreadyExists("Tutor emailId already exists.");
        }

        Tutor tutor= new Tutor();
        tutor.setTutorID(tutorID);
        tutor.setName(name);
        tutor.setTimeStamp(LocalDateTime.now());
        tutor.setEmailId(emailId);
        tutor.setPassword(bcryptEncoder.encode(password));
        tutor.setOccupation(occupation);
        tutor.setDegree(degree);
        tutor.setSubjects(subjects);
        tutorRepo.save(tutor);
        return tutor ;
    }

    @Override
    public Tutor updateTutor(String tutorID,String name,String emailId,String password, String occupation, String degree,String subjects) throws TutorNotFoundException{

        if(tutorRepo.findByEmailId(emailId)==null){
            throw new TutorNotFoundException("Tutor not found.");
        }
        Tutor tutor= new Tutor();
        tutor.setTutorID(tutorID);
        tutor.setName(name);
        tutor.setTimeStamp(LocalDateTime.now());
        tutor.setEmailId(emailId);
        tutor.setPassword(bcryptEncoder.encode(password));
        tutor.setOccupation(occupation);
        tutor.setDegree(degree);
        tutor.setSubjects(subjects);
        tutorRepo.save(tutor);
        return tutor ;
    }

    @Override
    public void deleteTutorByName(String emailId) throws TutorNotFoundException {

        Tutor tutor= tutorRepo.findByEmailId(emailId);
        if(tutor==null){
            throw new TutorNotFoundException("Tutor not found.");
        }
        tutorRepo.delete(tutor);
    }

    @Override
    public void deleteAll() {

       tutorRepo.deleteAll();

    }
}
